'use strict';

angular.module('metronicjhipsterApp')
	.directive('spinnerBar', ['$rootScope', function($rootScope) {
		return {
			link: function(scope, element, attrs) {
				element.addClass('hide');
				
				$rootScope.$on('$stateChangeStart', function() {
					element.removeClass('hide')
				});
				
				$rootScope.$on('$stateChangeSuccess', function() {
					element.addClass('hide');
					$('body').removeClass('page-on-load');
					Layout.setSidebarMenuActiveLink('match');
					
					setTimeout(function() {
						App.scrollTop();
					});
				});
				
				$rootScope.$on('$stateNotFound', function() {
					element.addClass('hide');
				});
				
				$rootScope.$on('$stateChangeError', function() {
					element.addClass('hide');
				});
			}
		};
	}])