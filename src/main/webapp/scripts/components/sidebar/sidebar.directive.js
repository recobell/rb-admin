'use strict';

angular.module('metronicjhipsterApp')
    .directive('initSidebar', function() {
    	return {
    		restrict: 'A',
    		link: function($scope) {
    			$scope.initSidebar();
    		}
    	}
    });