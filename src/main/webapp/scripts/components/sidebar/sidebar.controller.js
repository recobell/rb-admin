'use strict'

angular.module('metronicjhipsterApp')
	.controller('SidebarController', function($scope, $location, $state, Auth, Principal, ENV){
		$scope.$state = $state;
		$scope.inProduction = ENV === 'prod';
		$scope.isAuthenticated = Principal.isAuthenticated;
		
		$scope.initSidebar = Layout.initSidebar;
});