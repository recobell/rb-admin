'use strict';

angular.module('metronicjhipsterApp')
    .controller('NavbarController', function ($scope, $location, $state, Auth, Principal, ENV) {
        $scope.$state = $state;
        $scope.inProduction = ENV === 'prod';
        $scope.isAuthenticated = Principal.isAuthenticated;

        $scope.initHeader = Layout.initHeader;
        
        $scope.logout = function () {
            Auth.logout();
            $state.go('login');
        };
    });
