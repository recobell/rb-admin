'use strict';

angular.module('metronicjhipsterApp')
    .factory('errorHandlerInterceptor', function ($q, $rootScope) {
        return {
            'responseError': function (response) {
                if (!(response.status == 401 && response.data.path.indexOf("/api/account") == 0 )){
	                $rootScope.$emit('metronicjhipsterApp.httpError', response);
	            }
                return $q.reject(response);
            }
        };
    });