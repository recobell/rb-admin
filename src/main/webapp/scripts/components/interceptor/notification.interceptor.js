 'use strict';

angular.module('metronicjhipsterApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-metronicjhipsterApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-metronicjhipsterApp-params')});
                }
                return response;
            }
        };
    });
