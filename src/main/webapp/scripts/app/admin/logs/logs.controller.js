'use strict';

angular.module('metronicjhipsterApp')
    .controller('LogsController', function ($scope, LogsService) {
        $scope.loggers = LogsService.findAll();

        $scope.changeLevel = function (name, level) {
            LogsService.changeLevel({name: name, level: level}, function () {
                $scope.loggers = LogsService.findAll();
            });
        };
        
        $scope.$on('$viewContentLoaded', function() {
        	Layout.setSidebarMenuActiveLink('set', $('#logs'));
        	Layout.initContent();
        });
    });
