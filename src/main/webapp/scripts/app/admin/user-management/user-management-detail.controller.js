'use strict';

angular.module('metronicjhipsterApp')
    .controller('UserManagementDetailController', function ($scope, $stateParams, User) {
        $scope.user = {};
        $scope.load = function (login) {
            User.get({login: login}, function(result) {
                $scope.user = result;
            });
        };
        $scope.load($stateParams.login);
        
        $scope.$on('$viewContentLoaded', function() {
        	Layout.setSidebarMenuActiveLink('set', $('#user_management'));
        	Layout.initContent();
        });
    });
