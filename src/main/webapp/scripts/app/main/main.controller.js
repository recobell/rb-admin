'use strict';

angular.module('metronicjhipsterApp')
    .controller('MainController', function ($scope, Principal) {
        Principal.identity().then(function(account) {
            $scope.account = account;
            $scope.isAuthenticated = Principal.isAuthenticated;
        });
        
        $scope.$on('$viewContentLoaded', function() {
        	Layout.setSidebarMenuActiveLink('set', $('#home'));
        	Layout.initContent();
        });
    });
