/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.yello.metronicjhipster.web.rest.dto;
